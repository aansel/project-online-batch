﻿using Microsoft.ProjectServer.Client;
using Microsoft.SharePoint.Client;
using System.Security;

namespace project_online_batch.src.gateways
{

    /// <summary>
    /// Gateway to Project Online
    /// </summary>
    class ProjectOnlineGateway
    {
        private const string PROJECT_ONLINE_URL = "https://cern.sharepoint.com/sites/Projects-Accelerator-Complex-Schedules/";
        private const string PROJECT_ONLINE_USERNAME = "msppro@cern.ch";
        private const string PROJECT_ONLINE_PASSWORD = "pro2018@";
        
        
        /// <summary>
        /// Get list of projects from Project Online
        /// </summary>
        /// <returns></returns>
        public ProjectCollection getProjects()
        {
            ProjectContext projectContext = getProjectContext();
            projectContext.Load(projectContext.Projects);
            projectContext.ExecuteQuery();
            return projectContext.Projects;
        }


        /// <summary>
        /// Get Context allowing to fetch Data from Project Online
        /// </summary>
        /// <returns></returns>
        private ProjectContext getProjectContext()
        {
            SecureString securePassword = new SecureString();
            foreach (var c in PROJECT_ONLINE_PASSWORD.ToCharArray())
            {
                securePassword.AppendChar(c);
            }
            var projectContext = new ProjectContext(PROJECT_ONLINE_URL);
            projectContext.Credentials = new SharePointOnlineCredentials(PROJECT_ONLINE_USERNAME, securePassword);
            return projectContext;
        }
    }

}
