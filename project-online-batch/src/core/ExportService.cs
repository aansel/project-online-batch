﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using project_online_batch.src.dao;
using project_online_batch.src.gateways;
using Project = project_online_batch.src.dao.Project;
using project_online_batch.src.dao.batch;
using project_online_batch.src.core.enums;

namespace project_online_batch.src.core
{
    public class ExportService
    {

        /// <summary>
        /// Copy data from Project Online to our own database
        /// </summary>
        public void exportProjects()
        {

            // Fetch data from Project Online
            var projectOnlineGateway = new ProjectOnlineGateway();
            var projectCollection = projectOnlineGateway.getProjects();


            // Copy data to our own database
            using (var context = new CoordinationContext())
            {
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {

                    var success = true;
                    try
                    {
                        // Truncate project table
                        ProjectDAO projectDAO = new ProjectDAO();
                        projectDAO.truncate(context);

                        List<Project> dbProjects = new List<Project>();
                        foreach (var pubProj in projectCollection)
                        {
                            Debug.WriteLine("\n\t{0} :\n\t{1} : {2}", pubProj.Id.ToString(), pubProj.Name, pubProj.CreatedDate.ToString());
                            dbProjects.Add(new Project { Name = pubProj.Name, InterventionPeriod = "LS2" });
                        }

                        // Interception/SQL logging
                        context.Database.Log = (string message) => { Console.WriteLine(message); };
                        context.Projects.AddRange(dbProjects);

                        context.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch
                    {
                        success = false;
                        dbContextTransaction.Rollback();
                        throw;
                    }
                    finally
                    {
                        // Update Batch status
                        BatchDAO batchDAO = new BatchDAO();
                        Batch batch = batchDAO.getByName(context, BATCH.COPY_PROJECT_ONLINE_DATA.ToString());
                        batch.Status = success ? BATCH_STATUS.OK.ToString() : BATCH_STATUS.ERROR.ToString();
                        batch.ExecutionDate = DateTime.Now;
                        context.SaveChanges();
                    }
                }
            }
        }
    }
}
