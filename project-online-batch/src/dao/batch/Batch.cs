﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace project_online_batch.src.dao
{
    public class Batch
    {

        [Column("ID")]
        public int Id { get; set; }

        [Column("NAME")]
        public string Name { get; set; }

        [Column("STATUS")]
        public string Status { get; set; }

        [Column("EXECUTION_DATE")]
        public DateTime ExecutionDate { get; set; }

    }
}
