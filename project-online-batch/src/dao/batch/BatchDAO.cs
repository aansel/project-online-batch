﻿using System.Linq;

namespace project_online_batch.src.dao.batch
{
    class BatchDAO
    {

        /// <summary>
        /// Get a batch by its name
        /// </summary>
        /// <param name="context"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public Batch getByName(CoordinationContext context, string name)
        {
            return (
                from b in context.Batches
                where b.Name.Equals(name)
                select b
            )
            .First();
        }
    }

}
