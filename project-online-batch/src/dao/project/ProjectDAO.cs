﻿
namespace project_online_batch.src.dao.batch
{
    class ProjectDAO
    {

        /// <summary>
        /// Truncate the table
        /// </summary>
        /// <param name="context"></param>
        public void truncate(CoordinationContext context)
        {
            context.Database.ExecuteSqlCommand("TRUNCATE TABLE projects");
        }
    }
}
