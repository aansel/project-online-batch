﻿using System.ComponentModel.DataAnnotations.Schema;

namespace project_online_batch.src.dao
{
    public class Project
    {
        [Column("ID")]
        public int Id { get; set; }

        [Column("NAME")]
        public string Name { get; set; }

        [Column("INTERVENTION_PERIOD")]
        public string InterventionPeriod { get; set; }

    }
}
