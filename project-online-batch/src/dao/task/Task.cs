﻿using System.ComponentModel.DataAnnotations.Schema;

namespace project_online_batch.src.dao
{
    public class Task
    {
        [Column("ID")]
        public int Id { get; set; }

        [Column("NAME")]
        public string Name { get; set; }

    }
}
