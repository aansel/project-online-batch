﻿using MySql.Data.Entity;
using System.Data.Entity;

namespace project_online_batch.src.dao
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class CoordinationContext : DbContext
    {
        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Batch> Batches { get; set; }

        public CoordinationContext()
        {
            Database.SetInitializer<CoordinationContext>(null);
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Project>().ToTable("projects");
            modelBuilder.Entity<Task>().ToTable("tasks");
            modelBuilder.Entity<Batch>().ToTable("batches");
        }
    }
}
