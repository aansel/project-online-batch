﻿using project_online_batch.src.core;

namespace project_online_batch
{
    public class Program
    {
        private static void Main(string[] args)
        {
            var exportService = new ExportService();
            exportService.exportProjects();
        }
    }
}
