FROM mono
ADD . /src
WORKDIR /src
RUN mono nuget.exe restore
RUN xbuild /p:Configuration=Release
CMD [ "mono", "/src/project-online-batch/bin/Release/project-online-batch.exe" ]
